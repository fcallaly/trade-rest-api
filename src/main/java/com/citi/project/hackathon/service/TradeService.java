package com.citi.project.hackathon.service;

import java.util.List;

import com.citi.project.hackathon.dao.TradeMongoRepo;
import com.citi.project.hackathon.model.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the main business logic class for Employees.
 * 
 * @author Katharine
 * @see Employee
 */
@Component
public class TradeService {

    @Autowired
    private TradeMongoRepo mongoRepo;

    public List<Trade> findAll() {
        return mongoRepo.findAll();

    }

    public Trade save(Trade trade) {
        return mongoRepo.save(trade);
    }

    public Trade update(String id, Trade trade){
        trade.setId(id);

        return mongoRepo.save(trade);
    }

}
