package com.citi.project.hackathon.model;

public enum TradeState{

    CREATED, PENDING, VERIFIED, REJECTED

}
